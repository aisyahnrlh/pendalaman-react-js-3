import React from 'react';
import './App.css';
import Catalog from './components/Catalog';
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <div className="App">
      <div className="navbar-home">
        <a href="#" className="bar-item-button"><i class="fa fa-facebook-official"></i></a>
        <a href="#" className="bar-item-button"><i class="fa fa-instagram"></i></a>
        <a href="#" className="bar-item-button"><i class="fa fa-snapchat"></i></a>
        <a href="#" className="bar-item-button"><i class="fa fa-flickr"></i></a>
        <a href="#" className="bar-item-button"><i class="fa fa-twitter"></i></a>
        <a href="#" className="bar-item-button"><i class="fa fa-linkedin"></i></a>
      </div>

      <div className="jumbotron">
        <h1>DIGITALENT STORE</h1>
        <h6>Welcome to the store</h6>
      </div>

        <Catalog/>
    </div>
  );
}

export default App;
