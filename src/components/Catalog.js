import React, { Component } from 'react'
import { CSSTransitionGroup } from 'react-transition-group'

export default class Catalog extends Component {
    constructor() {
        super()

        this.state = {
            length: 0,
            products: []
        }
        this.handleAdd = this.handleAdd.bind(this);
    }
    

    componentDidMount() {
        fetch('https://fakestoreapi.com/products?limit=5')
            .then(res => res.json())
            .then(res => this.setState({
                length: res.length,
                products: res
            }))
    }

    handleAdd() {
        const newProducts = this.state.products.concat([
            { id: this.state.length + 1, title: prompt('Enter product name'), price: prompt('Enter product price') }
        ]);
        this.setState((state) => ({length: state.length + 1, products: newProducts }))
    }

    handleRemove(id) {
        let newProducts = this.state.products.filter(function (product) {
            return product.id !== id
        });
        this.setState({ products: newProducts })
    }

    render() {
        const { products } = this.state
        const productsItem = products.map((product) =>
            <div key={product.id}>
                <button class="btn btn-primary" onClick={this.handleAdd}><b>Add Item</b></button>
                <div class="d-flex align-items-stretch">
                    <img src={product.image} width="200px" height="200px"/>
                    <br/>
                    <b>{product.title}</b>
                    <br/>
                    Harga: Rp {product.price*15000}
                    <br/>
                    {product.description}
                    <br/>
                    Kategori: {product.category}
                    <hr/>
                    <button class="btn btn-danger" onClick={() => this.handleRemove(product.id)} style={{ color: "wnite", cursor: "pointer" }}><b>X</b></button>
                </div>
            </div>
        )

        return (
            <div style={{ textAlign: 'left'}}>
                
                <CSSTransitionGroup
                    transitionName="fade"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}>
                    {productsItem}
                </CSSTransitionGroup>
            </div>
        )
    }
}